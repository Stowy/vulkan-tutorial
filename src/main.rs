use anyhow::Result;
use log::*;
use vulkan_tutorial::app::App;
use vulkanalia::vk::DeviceV1_0;
use winit::dpi::LogicalSize;
use winit::event::{ElementState, Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::keyboard::{KeyCode, PhysicalKey};
use winit::window::WindowBuilder;

fn main() -> Result<()> {
    pretty_env_logger::init();

    let event_loop = EventLoop::new()?;
    let window = WindowBuilder::new()
        .with_title("Prout")
        .with_inner_size(LogicalSize::new(1024, 768))
        .build(&event_loop)?;

    let mut app = unsafe { App::create(&window)? };
    let mut destroying = false;
    let mut minimized = false;

    event_loop.set_control_flow(ControlFlow::Poll);
    event_loop.run(move |event, event_loop_window_target| match event {
        Event::WindowEvent {
            event: WindowEvent::CloseRequested,
            ..
        } => {
            destroying = true;
            unsafe {
                app.device.device_wait_idle().unwrap();
            }
            unsafe {
                app.destroy();
            }
            event_loop_window_target.exit();
            info!("Closing window...");
        }
        Event::AboutToWait if !destroying && !minimized => {
            unsafe { app.render(&window) }.unwrap();
        }
        Event::WindowEvent {
            event: WindowEvent::Resized(size),
            ..
        } => {
            if size.width == 0 || size.height == 0 {
                minimized = true;
            } else {
                minimized = false;
                app.resized = true
            }
        }
        Event::WindowEvent {
            event: WindowEvent::KeyboardInput { event, .. },
            ..
        } => {
            if event.state == ElementState::Pressed {
                match event.physical_key {
                    PhysicalKey::Code(KeyCode::ArrowLeft) if app.models > 1 => app.models -= 1,
                    PhysicalKey::Code(KeyCode::ArrowRight) if app.models < 4 => app.models += 1,
                    _ => {}
                }
            }
        }
        _ => {}
    })?;

    Ok(())
}
